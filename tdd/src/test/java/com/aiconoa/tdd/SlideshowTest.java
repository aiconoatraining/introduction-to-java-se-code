package com.aiconoa.tdd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SlideshowTest {
    
    private WebDriver driver;
    
    @Before
    public void setUp() {
        // à spécifier si Chrome Driver n'est pas dans le chemin par défaut https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver
        System.setProperty("webdriver.chrome.driver", "/Users/thomasgros/Downloads/chromedriver");
        driver = new ChromeDriver();
    }
    
    @After
    public void tearDown() {
        driver.quit();
    }
    
    @Test
    public void slideshowListShouldHaveATitle() {
        driver.get("http://localhost:8080/javaee7/faces/slideshow-jsf/slideshow-list.xhtml");
//        System.out.println(driver.getTitle());
        assertEquals("Slideshows", driver.getTitle());
    }
    
    @Test
    public void slideshowListShouldContainP() {
        driver.get("http://localhost:8080/javaee7/faces/slideshow-jsf/slideshow-list.xhtml");

        WebElement firstP = driver.findElement(By.tagName("p"));
        assertEquals("Les slideshow disponibles", firstP.getText());
    }
    
    @Test
    public void slideshowListShouldContain2GalleryItems() {
        driver.get("http://localhost:8080/javaee7/faces/slideshow-jsf/slideshow-list.xhtml");
        
        List<WebElement> galleryItems = driver.findElements(By.cssSelector(".slideshow-gallery__item"));
        assertEquals(2, galleryItems.size());
    }
    
    
    @Test
    public void clickOnGalleryItemShouldOpenSlideshowPlayerPage() {
        driver.get("http://localhost:8080/javaee7/faces/slideshow-jsf/slideshow-list.xhtml");
        WebElement firstGalleryItem = driver.findElement(By.cssSelector(".slideshow-gallery__item"));
        
        firstGalleryItem.click();
        
        assertEquals("Slideshow", driver.getTitle());
        
        WebElement svg = driver.findElement(By.tagName("svg"));
        assertNotNull(svg);
        
        WebElement hud = driver.findElement(By.cssSelector(".slideshow__hud"));
        assertNotNull(hud);
    }
    
    
    
    
    

}
