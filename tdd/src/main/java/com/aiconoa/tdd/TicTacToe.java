package com.aiconoa.tdd;

public class TicTacToe {

    private boolean drawGame = false;
    private boolean gameOver = false;
    
    private char currentPlayer = 'X';
    
    private Character winner;
    
    private Character[][] board = { { '\0', '\0', '\0' }, { '\0', '\0', '\0' }, { '\0', '\0', '\0' } };

    private void checkXAxis(int x, int y) {
        if (x < 1 || x > 3) {
            throw new RuntimeException("X is outside of board " + x);
        }
    }

    private void checkYAxis(int x, int y) {
        if (y < 1 || y > 3) {
            throw new RuntimeException("Y is outside of board " + y);
        }
    }

    private void checkIfOccupied(int x, int y) {
        if (board[x - 1][y - 1] != '\0') {
            throw new RuntimeException(String.format("Square already occupied (%d, %d)", x, y));
        }
    }

    private void switchCurrentPlayer() {
        if(currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }   
    }
    
    public void play(int x, int y) {
        checkXAxis(x, y);
        checkYAxis(x, y);
        checkIfOccupied(x, y);
        
        board[x - 1][y - 1] = this.currentPlayer;
        
        checkGameOverFullBoard();
        checkWinner(x , y);

        switchCurrentPlayer();
    }

    private void checkGameOverFullBoard() {
        for (Character[] characters : board) {
            for (Character character : characters) {
                if(character == '\0') { return; }
            }
        }
        
        this.drawGame = true;
        this.gameOver = true;
    }

    private void checkWinner(int x,int y) {
        // calcul du winner sur axe horizontal
        if(board[0][y - 1] == this.currentPlayer
        && board[1][y - 1] == this.currentPlayer
        && board[2][y - 1] == this.currentPlayer) {
            winner = this.currentPlayer;
            this.gameOver = true;
            return;
        } 
        
        // calcul du winner sur axe vertical
        if(board[x - 1][0] == this.currentPlayer
        && board[x - 1][1] == this.currentPlayer
        && board[x - 1][2] == this.currentPlayer) {
            winner = this.currentPlayer;
            this.gameOver = true;
            return;
        }
        
        // check bottom left - top right diagonal
        if (board[0][2] == this.currentPlayer
         && board[1][1] == this.currentPlayer
         && board[2][0] == this.currentPlayer) {
            winner = this.currentPlayer;
            this.gameOver = true;
            return;
        } 
        
        // check top left - bottom right diagonal
        if (board[0][0] == this.currentPlayer
         && board[1][1] == this.currentPlayer
         && board[2][2] == this.currentPlayer) {
            winner = this.currentPlayer;
            this.gameOver = true;
            return;
        }
        
    }

    public char getCurrentPlayer() {
        return this.currentPlayer;
    }

    public Character getWinner() {
        return winner;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isDrawGame() {
        return this.drawGame;
    }

}
