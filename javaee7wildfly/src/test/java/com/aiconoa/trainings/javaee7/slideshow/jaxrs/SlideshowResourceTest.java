package com.aiconoa.trainings.javaee7.slideshow.jaxrs;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowServiceEJB;

@RunWith(MockitoJUnitRunner.class)
public class SlideshowResourceTest {
    
    @Mock
    private SlideshowServiceEJB slideshowService;
    
    @InjectMocks
    private SlideshowResource slideshowResource;
    
    @Before
    public void setUp() {

        Slideshow slideshow = new Slideshow();
        slideshow.setId(1);
        slideshow.setTitle("mes vacances au pays du test");
        slideshow.setWidth(800);
        slideshow.setHeight(600);
        
        when(slideshowService.findSlideshow(1)).thenReturn(slideshow);
    }
    
    @Test
    public void testGetSlideshow() {
        String expected = "{\"id\":1,\"title\":\"mes vacances au pays du test\",\"width\":800,\"height\":600,\"slides\":[]}";

        String json = slideshowResource.getSlideshow(1);

        assertEquals(expected, json);        
    }

}
