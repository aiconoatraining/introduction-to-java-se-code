<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html">
<html>
<head>
<meta charset="UTF-8">
<title>Slideshow</title>
<link rel="stylesheet" href="slideshow/css/reset.css">
<link rel="stylesheet" href="slideshow/css/slideshow.css">
</head>
<body>
	<div class="container">
		<div class="slideshow">
			<h1 class="slideshow__title">${ slideshow.title }</h1>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                class="slideshow__slide"
                viewport="0 0 ${ slideshow.width } ${ slideshow.height }"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                
                <c:forEach items="${ slide.slideElements  }" var="e">
                
                <c:if test="${ e.class.name == 'com.aiconoa.trainings.javaee7.slideshow.model.jpa.TextSlideElement' }">
<text text-anchor="middle" alignment-baseline="middle" x="${ e.x }"  y="${ e.y }" font-family="Helvetica" font-size="40"  fill="black">${ e.content }</text>
                </c:if>
                <c:if test="${ e.class.name == 'com.aiconoa.trainings.javaee7.slideshow.model.jpa.ImageSlideElement' }">
<image xlink:href="/javaee7/img?img=${ e.content }" x="${ e.x }" y="${ e.y }" width="${ e.width }" height="${ e.height }" preserveAspectRatio="xMidYMid meet"/>                               
                </c:if>

                </c:forEach>
            </svg>

			<div class="slideshow__hud">
				<c:if test="${ slideIndex <= 0 }">
					<a class="slideshow__hud__link slideshow__hud__link--hidden"
						href="?id=${ slideshow.id }&index=${ slideIndex - 1 }">previous</a>
				</c:if>
				<c:if test="${ slideIndex > 0 }">
					<a class="slideshow__hud__link"
						href="?id=${ slideshow.id }&index=${ slideIndex - 1 }">previous</a>
				</c:if>

				<c:if test="${ slideIndex >= slideshow.size() - 1 }">
					<a class="slideshow__hud__link slideshow__hud__link--hidden"
						href="?id=${ slideshow.id }&index=${ slideIndex + 1 }">next</a>
				</c:if>
				<c:if test="${ slideIndex < slideshow.size() - 1 }">
					<a class="slideshow__hud__link"
						href="?id=${ slideshow.id }&index=${ slideIndex + 1 }">next</a>
				</c:if>

				<p class="slideshow__hud__index">${ slideIndex + 1 }/ ${ slideshow.size() }</p>
            </div>
        </div>
    </div>
</body>
</html>