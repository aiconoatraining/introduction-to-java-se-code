<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>UsernamePassword JSP</title>
</head>
<body>
<form action="/javaee7/usernamepassword" method="POST">
  <label for="username">Username</label>
  <input id="username" type="text" name="username" value="${ requestScope.submittedRawData['username'] }"/>
  <c:if test="${ not empty requestScope.errors['username'] }">
    <span>${ requestScope.errors['username'] }</span>
  </c:if>
  
  <label for="password">Password</label>
  <input id="password" type="password" name="password"/>
  <c:if test="${ not empty requestScope.errors['password'] }">
    <span>${ requestScope.errors['password'] }</span>
  </c:if>

  <input type="submit" />
</form>
</body>
</html>