<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Hello JSP</title>
</head>
<body>
<c:if test="${ requestScope.random }">
    <p>${ requestScope.polite } ${ requestScope.who }</p>
</c:if>
<ul>
<c:forEach items="${ requestScope.data }" var="item">
    <li>${ item }</li>
</c:forEach>
</ul>
</body>
</html>