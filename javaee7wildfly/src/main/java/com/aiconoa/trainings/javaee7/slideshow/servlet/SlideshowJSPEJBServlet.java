package com.aiconoa.trainings.javaee7.slideshow.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowServiceEJB;

@WebServlet("slideshowjspejb")
public class SlideshowJSPEJBServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(SlideshowJSPEJBServlet.class.getName());
    
    @Inject
    private SlideshowServiceEJB slideshowService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        LOGGER.info("SlideshowServlet - doGet - " + req.getQueryString());

        String slideshowIdParam = req.getParameter("id");
        
        if(slideshowIdParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideshowIdParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "id doit être un entier");
            return;
        }
        
        int slideshowId = Integer.parseInt(slideshowIdParam);
        
        Slideshow slideshow = slideshowService.findSlideshow(slideshowId);
        
        if(slideshow == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }   
        
        String slideIndexParam = req.getParameter("index");
        if(slideIndexParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideIndexParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "index doit être un entier");
            return;
        }
        
        int slideIndex = Integer.parseInt(slideIndexParam);
        
        
        if(slideIndex < 0 || slideIndex >= slideshow.size()) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        Slide slide = slideshow.getSlideAt(slideIndex);
        
        req.setAttribute("slideshow", slideshow);

        req.setAttribute("slide", slide);
        req.setAttribute("slideIndex", slideIndex);
        
        LOGGER.info("##########################################");
        
        LOGGER.info(slideshow.toString());
        for(Slide s: slideshow.getSlides()) {
            LOGGER.info(s.toString());
            for(SlideElement<?> se: s.getSlideElements()) {
                LOGGER.info(se.toString());
            }
        }
        
        LOGGER.info("##########################################");
        
        req.getRequestDispatcher("/WEB-INF/slideshow/slideshowejb.jsp")
            .forward(req, resp);
    }
    
}
