package com.aiconoa.trainings.javaee7.ejb;

import javax.ejb.Singleton;

/**
 * Session Bean implementation class SingletonEJB
 */
@Singleton
public class SingletonEJB {

    private double value = Math.random();
    
    public double getValue() {
        return value;
    }

}
