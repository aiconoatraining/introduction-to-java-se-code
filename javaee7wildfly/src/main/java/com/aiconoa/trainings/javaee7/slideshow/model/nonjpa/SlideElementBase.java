package com.aiconoa.trainings.javaee7.slideshow.model.nonjpa;

import java.util.Objects;

public abstract class SlideElementBase<T> implements SlideElement<T> {

    private double x;
    private double y;
    private double width;
    private double height;
    private T content;

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public double getWidth() {
        return width;
    }

    /**
     * @throws IllegalArgumentException if width < 0
     */
    @Override
    public void setWidth(double width) {
        if(width < 0) {
            // throw new RuntimeException("width must be > 0");
            throw new IllegalArgumentException("width must be > 0");
        }
        this.width = width;
    }

    @Override
    public double getHeight() {
        return height;
    }

    /**
     * @throws IllegalArgumentException if height < 0
     */
    @Override
    public void setHeight(double height) {
        if(height < 0) {
            throw new IllegalArgumentException("height must be > 0");
        }
        
        this.height = height;
    }

    @Override
    public T getContent() {
        return content;
    }

    /**
     * @throws NullPointerException if content is null
     */
    public void setContent(T content) {
        Objects.requireNonNull(content, "content must not be null");
        this.content = content;
    }

    @Override
    public String toString() {
        return "SlideElementBase [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + ", content="
                + content + "]";
    }

}
