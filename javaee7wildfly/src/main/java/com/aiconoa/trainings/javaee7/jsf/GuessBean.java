package com.aiconoa.trainings.javaee7.jsf;

import java.io.Serializable;
import java.util.Random;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named 
@ViewScoped // ATTENTION NE PAS PRENDRE javax.faces.beans.ViewScoped  sinon incompatible avec CDI
public class GuessBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(GuessBean.class.getName());
    
    private Integer number;
    
    private Integer solution;
    
    private boolean success;
    
    public boolean isPostback() {
        return FacesContext.getCurrentInstance().isPostback();
    }
    
    public GuessBean() {
        logMethodPhase("GuessBean#constructor");
        Random random = new Random();
        solution = random.nextInt(10) + 1;
        LOGGER.info(String.format("solution: %d", solution));
    }
    
    private void logMethodPhase(String methodName) {
        LOGGER.info(String.format("%s called in phase %s", 
                methodName, 
                FacesContext.getCurrentInstance().getCurrentPhaseId()));
    }
    
    // #PHASE UPDATE MODEL VALUE ?
    public void setNumber(Integer number) {
        logMethodPhase("GuessBean#setNumber");
        this.number = number;
    }
    
    // #PHASE INVOKE APPLICATION ?
    // return void pour rester sur la même "view"
    public void guess() {
        logMethodPhase("GuessBean#guess");
        LOGGER.info(String.format("GuessBean#guess called with number %d", number));
        
        this.success = this.solution.equals(this.number);
        LOGGER.info(String.format("GuessBean#guess success %s", this.success));
    }
    
    // #PHASE RENDER RESPONSE ?
    public Integer getNumber() {
        logMethodPhase("GuessBean#getNumber");
        return number;
    }
    
    public Boolean getSuccess() {
        LOGGER.info(String.format("GuessBean#getSuccess success %s", this.success));
        return success;
    }
}
