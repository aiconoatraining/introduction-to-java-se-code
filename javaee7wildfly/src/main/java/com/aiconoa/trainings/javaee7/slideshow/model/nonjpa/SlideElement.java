package com.aiconoa.trainings.javaee7.slideshow.model.nonjpa;

import java.io.Serializable;

public interface SlideElement<T> extends Serializable {

    double getX();

    void setX(double x);

    double getY();

    void setY(double y);

    double getWidth();

    /**
     * @throws IllegalArgumentException if width < 0
     */
    void setWidth(double width);

    double getHeight();

    /**
     * @throws IllegalArgumentException if height < 0
     */
    void setHeight(double height);

    T getContent();

    /**
     * @throws NullPointerException
     *             if content is null
     * @throws IllegalArgumentException
     *             if the content type is not acceptable for a specific SlideElement
     *             implementation
     */
    void setContent(T content);

}
