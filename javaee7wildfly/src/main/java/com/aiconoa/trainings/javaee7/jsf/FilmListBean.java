package com.aiconoa.trainings.javaee7.jsf;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.javaee7.ejb.FilmService;
import com.aiconoa.trainings.javaee7.entity.Film;

@Named
@RequestScoped
public class FilmListBean {
    private static Logger LOGGER = Logger.getLogger(FilmListBean.class.getName());

    @Inject
    private FilmService filmService;
    
    private List<Film> filmListCache;

    // 1 constructeur du bean est appelé
    // 2 les dependances sont résolues
    // 3 @PostConstruct
    @PostConstruct
    private void init() {
        filmListCache = filmService.findAllFilms();
    }

    public List<Film> getFilms() {
        LOGGER.info("FilmListBean#getFilms");
        return filmListCache;
    }
    
}
