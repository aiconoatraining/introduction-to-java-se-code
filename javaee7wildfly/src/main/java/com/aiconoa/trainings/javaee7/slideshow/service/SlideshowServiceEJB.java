package com.aiconoa.trainings.javaee7.slideshow.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;

@Stateless
public class SlideshowServiceEJB {

    @PersistenceContext(unitName="SlideshowPU")
    private EntityManager em;

   
    // Toutes les méthodes des EJBs session s'exécutent dans un contexte transactionnel
    // Les EJB wrappent les RuntimeException dans des EJBException
    /**
     * @param id
     * @return the corresponding Slideshow. null if id doesn't exist.
     */
    public Slideshow findSlideshow(int id) { 
        
        TypedQuery<Slideshow> query = 
                em.createQuery("SELECT DISTINCT s FROM Slideshow s WHERE s.id = :id", Slideshow.class);
        query.setParameter("id", id);        
        query.setHint("javax.persistence.loadgraph", em.createEntityGraph("graph.Slideshow.slides.slideelements"));

        try {
            return query.getSingleResult();
        } catch(NoResultException ex) {
            return null;
        }
    }
    
    public List<Slideshow> findAllSlideshow() {
        TypedQuery<Slideshow> query = em
                .createQuery("SELECT s FROM Slideshow s", Slideshow.class);
        return query.getResultList();
    }
    
    // Si jamais il y a beaucoup de combinaisons / de variantes de critères de requêtes
    // ou que on doit les générer "dynamiquement" => s'intéresser à la Criteria API
    // https://en.wikibooks.org/wiki/Java_Persistence/Criteria
    public List<Slideshow> findAllSlideshowWithSlidesAndSlideElements() {
        TypedQuery<Slideshow> query = em
                .createQuery("SELECT DISTINCT s FROM Slideshow s", Slideshow.class);
        query.setHint("javax.persistence.loadgraph", em.createEntityGraph("graph.Slideshow.slides.slideelements"));
        return query.getResultList();
    }
    
    public String findFirstImageInASlideOfTheSlideshow(int id) {
        
       Query query = em.createNativeQuery("SELECT slideelement.content FROM slideshow INNER JOIN slide ON slideshow.id = slide.slideshow_id AND slideshow.id = :id INNER JOIN slideelement ON slide.id = slideelement.slide_id AND slideelement.type = 'image' ORDER BY slide.position LIMIT 1");
       query.setParameter("id", id);
       try {
           return query.getSingleResult().toString();
       } catch(NoResultException ex) {
           return null;
       }
    }

    public void createSlideshow(Slideshow slideshow) {
        em.persist(slideshow);
    }
    
    
}
