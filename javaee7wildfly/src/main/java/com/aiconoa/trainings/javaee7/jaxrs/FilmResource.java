package com.aiconoa.trainings.javaee7.jaxrs;

import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aiconoa.trainings.javaee7.ejb.FilmService;
import com.aiconoa.trainings.javaee7.entity.Film;

//http://localhost:8080/javaee7/sakila-rest-api/films
@Path("/films")
public class FilmResource {

    @Inject
    private FilmService filmService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String films() {
        List<Film> films = filmService.findAllFilms();

        // JSON Processing, JSR 353
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        
        for (Film film : films) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("id", film.getId());
            jsonObjectBuilder.add("title", film.getTitle());  
            
            jsonArrayBuilder.add(jsonObjectBuilder);
        }

        return jsonArrayBuilder.build().toString();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}") // http://localhost:8080/javaee7/sakila-rest-api/films/:id
    public String filmById(@PathParam("id") Integer id) {
        Film film = filmService.findById(id);
        
        if(film == null) {
            throw new NotFoundException();
            // throw new WebApplicationException(404);
        }  
        
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        jsonObjectBuilder.add("id", film.getId());
        jsonObjectBuilder.add("title", film.getTitle());  

        return jsonObjectBuilder.build().toString();    
    }
   
    
}
