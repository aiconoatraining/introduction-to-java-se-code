package com.aiconoa.trainings.javaee7.slideshow.model.nonjpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Slideshow implements Serializable {

    private static final long serialVersionUID = 1L;
  
    private String title;
    private int width;
    private int height;
    
    private ArrayList<Slide> slides;

    public Slideshow() {
        this.slides = new ArrayList<>();
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @throws NullPointerException if slide is null
     */
    public void addSlide(Slide slide) {
        Objects.requireNonNull(slide, "slide must not be null");
        slides.add(slide);
    }

    /**
     * @throws NullPointerException if slide is null
     */
    public void removeSlide(Slide slide) {
        Objects.requireNonNull(slide, "slide must not be null");
        slides.remove(slide);
    }
    
    public int size() {
        return slides.size();
    }
    
    public Slide[] getSlides() {
       return slides.toArray(new Slide[0]);
    }

    /**
     * @throws IndexOutOfBoundsException if index is < 0 or >= number of slides
     */
    private void rangeCheck(int index) {
        if(index < 0 || index >= slides.size()) {
            throw new IndexOutOfBoundsException(
                    String.format("Index: %d, Size: %d", index, slides.size()));
        }
    }

    /**
     * @throws IndexOutOfBoundsException if currentSlideIndex is < 0 or >= number of slides
     */
    public Slide getSlideAt(int currentSlideIndex) {
        rangeCheck(currentSlideIndex);

        return slides.get(currentSlideIndex);
    }

    @Override
    public String toString() {
        return "Slideshow [slides=" + slides + ", title=" + title + "]";
    }

}
