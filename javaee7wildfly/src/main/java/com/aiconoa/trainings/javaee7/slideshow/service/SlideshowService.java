package com.aiconoa.trainings.javaee7.slideshow.service;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.ImageSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.TextSlideElement;

public class SlideshowService {

    @PersistenceUnit(unitName="SlideshowPU")
    private EntityManagerFactory emf;
    
    @Resource
    private UserTransaction utx;

    public Slideshow findSlideshow(int id) {
        String[][] data = {
                {
                    "Taishan",
                    "taishan.jpg"
                },
                {
                    "Muine",
                    "muine.jpg"
                },
                {
                    "Angkor",
                    "angkor_monks.jpg"
                },
        };
        
        Slideshow slideshow = new Slideshow();
        slideshow.setTitle("Mes vacances en Asie");
        slideshow.setWidth(800);
        slideshow.setHeight(600);
        
        for (String[] d : data) {
            Slide s = new Slide();
            TextSlideElement tse = new TextSlideElement();
            tse.setX(400);
            tse.setY(100);
            tse.setWidth(800);
            tse.setHeight(50);
            tse.setContent(d[0]);
            s.addSlideElement(tse);
            
            ImageSlideElement ise = new ImageSlideElement();
            ise.setX(50);
            ise.setY(150);
            ise.setWidth(700);
            ise.setHeight(450);
            ise.setContent(d[1]);
            s.addSlideElement(ise);
            
            slideshow.addSlide(s);
        }
        
        return slideshow;
    }
}
