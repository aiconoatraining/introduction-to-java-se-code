package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("main")
public class MainServlet extends HttpServlet {
    
    private static Logger LOGGER = Logger.getLogger(MainServlet.class.getName());
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // System.out.println("coucou :-)"); // bof, préférer un logger !
        LOGGER.info("coucou depuis mon logger !");
        
        resp.setCharacterEncoding("utf-8");
        resp.addHeader("Content-Type", "text/html");
        
        resp.getWriter().append("<!DOCTYPE html>")
                        .append("<html>")
                            .append("<head>")
                                .append("<meta charset=\"utf-8\">")
                                .append("<title>Accueil</title>")
                            .append("</head>")
                            .append("<body>")
                                .append("<img src=\"img/duke.png\" style=\"width:100px\">")
                                .append("<br>")
                                .append("<a href=\"hello?name=thomas\">hello thomas</a>")
                                .append("<a href=\"pdf\">Download a pdf</a>")
                            .append("</body>")
                        .append("</html>");
    }
    
}
