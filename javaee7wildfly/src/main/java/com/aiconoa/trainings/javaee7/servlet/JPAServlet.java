package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.aiconoa.trainings.javaee7.entity.Film;

@WebServlet("jpa")
public class JPAServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(JPAServlet.class.getName());
        
    @PersistenceUnit(unitName="SakilaPU")
    private EntityManagerFactory emf; // thread-safe
    
    @Resource
    private UserTransaction utx;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        EntityManager em = emf.createEntityManager(); // em.getDelegate() ==> Session hibernate 

        try {
            utx.begin(); //              em.getTransaction().begin(); // PAS LE DROIT CAR JTA DATA SOURCE !!!

            TypedQuery<Film> query = em.createQuery("FROM Film", Film.class);
            List<Film> films = query.getResultList();

            for (Film film : films) {
                LOGGER.info(film.toString());
            }

            utx.commit();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error during the transaction", e);
            try {
                utx.rollback();
            } catch (Exception e2) {
                LOGGER.log(Level.SEVERE, "Error during transaction rollback", e2);
            }
        }

        em.close();
    }

}
