package com.aiconoa.trainings.javaee7.slideshow.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

//http://localhost:8080/javaee7/slideshow-api/...
@ApplicationPath("/slideshow-api")
public class SlideshowRestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        
        classes.add(SlideshowResource.class);
        
        return classes;
    }
}
