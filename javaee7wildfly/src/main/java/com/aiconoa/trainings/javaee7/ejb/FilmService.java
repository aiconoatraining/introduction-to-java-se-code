package com.aiconoa.trainings.javaee7.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aiconoa.trainings.javaee7.entity.Film;

@Stateless
public class FilmService {

    @PersistenceContext(unitName="SakilaPU")
    private EntityManager em;
    
    public List<Film> findAllFilms() {
        return em.createQuery("SELECT f FROM Film f", Film.class)
                .getResultList();
    }
    
    public Film findById(int id) {
        return em.find(Film.class, id);
    }
    
}
