package com.aiconoa.trainings.javaee7.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

// http://localhost:8080/javaee7/sakila-rest-api/...
@ApplicationPath("/sakila-rest-api")
public class SakilaRestApplication extends Application {

    // si pas surchargé alors les resources sont scannées automatiquement
    // (ou si le Set retourné est vide)
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        
        classes.add(HelloResource.class);
        classes.add(FilmResource.class);
        
        return classes;
    }
    
}
