package com.aiconoa.trainings.javaee7.slideshow.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;

@WebServlet("slideshowjpa")
public class SlideshowJPAServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(SlideshowJPAServlet.class.getName());
    
    @PersistenceUnit(unitName="SlideshowPU")
    private EntityManagerFactory emf;
    
    @Resource
    private UserTransaction utx;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManager em = emf.createEntityManager(); 

        try {
            utx.begin();

            Slideshow slideshow = em.find(Slideshow.class, 1);

            LOGGER.info(slideshow.toString());
            for(Slide s: slideshow.getSlides()) {
                LOGGER.info(s.toString());
                for(SlideElement<?> se: s.getSlideElements()) {
                    LOGGER.info(se.toString());
                }
            }

            utx.commit();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error during the transaction", e);
            try {
                utx.rollback();
            } catch (Exception e2) {
                LOGGER.log(Level.SEVERE, "Error during transaction rollback", e2);
            }
        }

        em.close();
    }
    
}
