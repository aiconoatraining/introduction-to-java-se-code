package com.aiconoa.trainings.javaee7.slideshow.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowServiceEJB;

@Named
@RequestScoped
public class SlideshowListBean {
    
    private static Logger LOGGER = Logger.getLogger(SlideshowListBean.class.getName());
    
    @Inject
    private SlideshowServiceEJB slideshowService;
    
    private List<Slideshow> slideshowListCache;
    
    private List<String> slideshowThumbnails;

    @PostConstruct
    private void init() {
        slideshowListCache = slideshowService.findAllSlideshow();
        
        slideshowThumbnails = new ArrayList<String>();
        for (Slideshow slideshow : slideshowListCache) {
            // can optimize this => one request fetches all thumbnails
            slideshowThumbnails.add(slideshowService.findFirstImageInASlideOfTheSlideshow(slideshow.getId()));
        }
    }

    public List<Slideshow> getSlideshows() {
        LOGGER.info("SlideshowListBean#getSlideshows");
        return slideshowListCache;
    }
    
    public List<String> getSlideshowThumbnails() {
        return slideshowThumbnails;
    }

}
