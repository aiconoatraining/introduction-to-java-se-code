package com.aiconoa.trainings.javaee7.slideshow.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.javaee7.servlet.filter.LogFilter;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.ImageSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.TextSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowService;

@WebServlet("slideshowjsp")
public class SlideshowJSPServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(SlideshowJSPServlet.class.getName());
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("SlideshowServlet - doGet - " + req.getQueryString());
        
        SlideshowService slideshowService = new SlideshowService();

        String slideshowIdParam = req.getParameter("id");
        
        if(slideshowIdParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideshowIdParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "id doit être un entier");
            return;
        }
        
        int slideshowId = Integer.parseInt(slideshowIdParam);
        
        Slideshow slideshow = slideshowService.findSlideshow(slideshowId);
        if(slideshow == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }   
        
        String slideIndexParam = req.getParameter("index");
        if(slideIndexParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideIndexParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "index doit être un entier");
            return;
        }
        
        int slideIndex = Integer.parseInt(slideIndexParam);
        
        
        if(slideIndex < 0 || slideIndex >= slideshow.size()) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        Slide slide = slideshow.getSlideAt(slideIndex);
        
        req.setAttribute("slideshow", slideshow);
        req.setAttribute("slide", slide);
        req.setAttribute("slideIndex", slideIndex);
        
        req.getRequestDispatcher("/WEB-INF/slideshow/slideshow.jsp")
            .forward(req, resp);
    }
    
}
