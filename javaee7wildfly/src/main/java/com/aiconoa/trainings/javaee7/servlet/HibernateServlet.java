package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
//import java.util.List;
//import java.util.logging.Logger;
//
//import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.aiconoa.trainings.javaee7.HibernateUtil;
import com.aiconoa.trainings.javaee7.entity.Film;

@WebServlet("hibernate")
public class HibernateServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(HibernateServlet.class.getName());
    
    private SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Session session = sessionFactory.openSession();

        session = sessionFactory.openSession();
        session.beginTransaction();

        Query query = session.createQuery("FROM Film");
        
        List<Film> films = (List<Film>) query.list();

        for (Film film : films) {
            LOGGER.info(film.toString());
        }

        session.getTransaction().commit();
        session.close();
    }

}
