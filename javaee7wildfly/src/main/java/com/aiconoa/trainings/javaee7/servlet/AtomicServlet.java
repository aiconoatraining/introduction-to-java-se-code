package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("atomic")
public class AtomicServlet extends HttpServlet {
 
    // private int counter = 0;
    private AtomicInteger counter = new AtomicInteger(0);
    
    
    private static final Logger LOGGER = Logger.getLogger(AtomicServlet.class.getName());
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Plusieurs Thread qui vont accéder à la servlet en même temps
        // ~ 1 thread / HTTP request

        // counter++;
        // LOGGER.info("counter: " + counter);

        int localCounter = counter.incrementAndGet();
        
        LOGGER.info("counter value: " + localCounter);
        LOGGER.info("hashcode: " + this.hashCode()); // Meme instance de Servlet

    }
    
}
