package com.aiconoa.trainings.javaee7.jsf;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

// il faut donner un nom au bean pour pouvoir l'utiliser dans l'expression language
// par défaut nom de la classe en camelCase => helloBean
@Named 
@RequestScoped // NE PAS PRENDRE javax.faces.beans.RequestScoped, SINON ON UTILISE PAS CDI
public class HelloBean {
    
    private String name = "Thomas";
    
    public String getName() {
        return name;
    }
    
}
