package com.aiconoa.trainings.javaee7.servlet.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

// disabled for now
// @WebFilter("/*")
public class LogFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(LogFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOGGER.info(String.format("doFilter - before chain - %s - %s", 
                ((HttpServletRequest) request).getServletPath(),
                ((HttpServletRequest) request).getPathInfo()));
       
        chain.doFilter(request, response);
        
        LOGGER.info(String.format("doFilter - after chain - %s - %s", 
                ((HttpServletRequest) request).getServletPath(),
                ((HttpServletRequest) request).getPathInfo()));
    }

    @Override
    public void destroy() {
        LOGGER.info("destroy");
    }

}
