package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("usernamepassword")
public class UsernamePasswordServlet extends HttpServlet {
    
    private static final Logger LOGGER = Logger.getLogger(UsernamePasswordServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/usernamepassword.jsp")
            .forward(req, resp);

    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> submittedRawData = new HashMap<>();
        
        String usernameParam = req.getParameter("username");
        submittedRawData.put("username", usernameParam);

        String passwordParam = req.getParameter("password");
        submittedRawData.put("password", passwordParam);

        Map<String, String> errors = new HashMap<>();
        
        if( usernameParam == null
         || usernameParam.isEmpty()) {
            errors.put("username", "Username must be specified");
        }
        
        if( passwordParam == null
        ||  passwordParam.length() < 8) {
            errors.put("password", "Password must be at least 8 characters");
        }
        
        LOGGER.info(errors.get("username"));
        LOGGER.info(errors.get("password"));
        
        if(! errors.isEmpty()) {
            req.setAttribute("submittedRawData", submittedRawData);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/usernamepassword.jsp")
                .forward(req, resp);
            return;
        }
        
        resp.sendRedirect("/javaee7/main"); // 302
    }
    
}
