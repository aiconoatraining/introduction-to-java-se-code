package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("hellojsp")
public class HelloJSPServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        req.setAttribute("random", Math.random() > 0.5 ? true: false);
        req.setAttribute("polite", "hello"); // pour transmettre la variable name à la JSP
        req.setAttribute("who", name); // pour transmettre la variable name à la JSP
        
        req.setAttribute("data", Arrays.asList(1,2,3,4,5));
        
        req.getRequestDispatcher("/WEB-INF/hello.jsp")
            .forward(req, resp);   
    }
    
}
