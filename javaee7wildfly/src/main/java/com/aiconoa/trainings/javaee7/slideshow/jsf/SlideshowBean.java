package com.aiconoa.trainings.javaee7.slideshow.jsf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.ImageSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.TextSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowServiceEJB;

@Named
@SessionScoped
public class SlideshowBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private static Logger LOGGER = Logger.getLogger(SlideshowBean.class.getName());
    
    @Inject
    private SlideshowServiceEJB slideshowService;
    
    private Integer id;
    
    transient private Integer index;

    private Slideshow slideshow;
    private Slide slide;
    
    private Map<Integer, Integer> lastVisitedSlidePerSlideshow = new HashMap<>();
    
    public Integer getId() {
        return id;
    }
    
    public Integer getIndex() {
        return index;
    }
    
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setIndex(Integer index) {
        this.index = index;
    }
    
    public String load() {
        LOGGER.info(String.format("SlideshowBean#load %d %d",id, index ));
        if (this.index == null) { // si pas d'index => rediriger vers le dernier index connu ou 0
            LOGGER.info(String.format("SlideshowBean#load %d %d",id, index ));
            return String.format("/slideshow-jsf/slideshow.xhtml?faces-redirect=true&id=%s&index=%s", 
                    this.id, lastVisitedSlidePerSlideshow.getOrDefault(this.id, 0));
            // FacesContext.getCurrentInstance().getExternalContext().redirect()
        }
        
        this.slideshow = slideshowService.findSlideshow(this.id);
        this.slide = slideshow.getSlideAt(this.index);
        this.lastVisitedSlidePerSlideshow.put(this.id, this.index);
        
        return null;
    }
    
    public Slideshow getSlideshow() {
        return slideshow;
    }
    
    public Slide getSlide() {
        return slide;
    }
    
    public String generateHTMLMarkupForSlideElement(SlideElement<?> se) {
        if(se instanceof TextSlideElement) {
            TextSlideElement e = (TextSlideElement) se;
            return String.format("<text text-anchor=\"middle\" alignment-baseline=\"middle\" x=\"%f\"  y=\"%f\" font-family=\"Helvetica\" font-size=\"40\"  fill=\"black\">%s</text>", e.getX(), e.getY(), e.getContent());                    
        } else if (se instanceof ImageSlideElement) {
            ImageSlideElement e = (ImageSlideElement) se;            
            return String.format("<image xlink:href=\"/javaee7/img?img=%s\" x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" preserveAspectRatio=\"xMidYMid meet\"/>", e.getContent(), e.getX(), e.getY(), e.getWidth(), e.getHeight());
        }
        
        return "";
    }
    
    
    
}
