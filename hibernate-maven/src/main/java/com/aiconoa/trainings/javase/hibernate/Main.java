package com.aiconoa.trainings.javase.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import com.aiconoa.trainings.javase.hibernate.entity.Film;

public class Main {
   
    public static void main(String[] args) throws InterruptedException {
        SessionFactory sessionFactory = null;
        
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            e.printStackTrace();
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
        }
        
 
     // create a couple of films...
        Session session = sessionFactory.openSession();
//        session.beginTransaction();
//
//        Film f = new Film();
//        f.setTitle("hibernate c'est trop bien");
//        f.setLanguageId(1);
//        
//        session.save(f);
//        
//        session.getTransaction().commit();
//        session.close();

        // now lets pull films from the database and list them
//        session = sessionFactory.openSession();
//        session.beginTransaction();
//        List<Film> result = session.createQuery( "from Film" , Film.class).getResultList();
//        for (Film film : result) {
//            System.out.println( film);
//            System.out.println( film.getActors());
//        }
//        session.getTransaction().commit();
//        session.close();
        
//      session = sessionFactory.openSession();
//      session.beginTransaction();
//          
//      Film film = session.find(Film.class, 1);
//      System.out.println(film);
//
//      Language newLanguage = new Language();
//      newLanguage.setName("Swahili");
//      session.save(newLanguage);
//
//      Film newFilm = new Film();
//      newFilm.setTitle("un nouveau film");
//      newFilm.setLanguageId(newLanguage);      
//      session.save(newFilm);
//      
//      session.getTransaction().commit();
//      session.close();

      session = sessionFactory.openSession();
      session.beginTransaction();
       
      Query<Film> query = session.createQuery("SELECT f FROM Film f", Film.class);
      // Query<Film> query = session.createQuery("FROM Film", Film.class);
      List<Film> films = query.getResultList();
      
      for (Film film : films) {
        // System.out.println(film);
      }
      
      Query<Film> query2 = session.createQuery("SELECT f FROM Film f WHERE f.id = :filmId", Film.class);
      query2.setParameter("filmId", 1001);
      
      Film film1001 = query2.getSingleResult(); // Attentions aux NoResultException, NonUniqueResultException,...
      System.out.println(film1001);
            
      Film film = session.find(Film.class, 1001); 
      session.getTransaction().commit();
      session.close();

        
    }
}