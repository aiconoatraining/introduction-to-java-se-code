package com.thomas_gros.javase.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {

    @Test
    public void addPositiveNumbers() {
        // GIVEN
        int a = 3;
        int b = 5;
        Calculator calculator = new Calculator();
        
        // WHEN
        int result = calculator.add(a,b);
        
        // THEN
        // result doit être égal à 8
        assertEquals(8, result); 
    }
    
    
    @Test(expected=CalculatorException.class)
    public void addBigNumbers() {
        // GIVEN
        int a = Integer.MAX_VALUE;
        int b = 1;
        Calculator calculator = new Calculator();
        
        // WHEN
        int result = calculator.add(a,b);  
    }
    
    
    
    
    @Test
    public void addNegativeNumbers() {
        int a = -3;
        int b = -5;
        
        Calculator calculator = new Calculator();
        
        int result = calculator.add(a, b);
        assertEquals(-8, result); 
        
    }

}
