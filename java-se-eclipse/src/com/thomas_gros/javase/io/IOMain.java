package com.thomas_gros.javase.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IOMain {

    public static void main(String[] args) throws IOException {
        // Input / Output Streams
        
        // Stream = flux = séquence de données, de bytes
            // notion abstraite: peut représenter plusieurs types de sources
            // et de destination: fichiers, périphériques, d'autres programmes,
            // zones de données en mémoire,...
        
        // Flux d'entrée représentées par une source d'entrée Input Stream
            // => lire un item à la fois
        // java.io.InputStream
        // https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html
        
        // Flux de sortie représentés par une destination de sortie Output Stream
            // => écrire un item à la fois
        // java.io.OutputStream https://docs.oracle.com/javase/8/docs/api/java/io/OutputStream.html

// lecture byte par byte du flux d'entrée standard, les bytes sont stockés dans un buffer
//        final int BUFFER_CAPACITY = 4096; // 1ko => 1024, 4k => 4096
//        byte[] bytesBuffer = new byte[BUFFER_CAPACITY];
//        int i = 0;
//          
//        int b;
//        while((b = System.in.read()) != -1) {
//            bytesBuffer[i] = (byte) b; // b est entre 0 et 255
//            i++;
//            
//            if(i == bytesBuffer.length) {
//                // choice 1)
//                    // processBuffer
//                    // clearBuffer
//                // choice 2)
//                    // increase buffer size
//                byte[] tmpBuffer = new byte[allBytesBuffer.length + BUFFER_CAPACITY];
//                System.arraycopy(allBytesBuffer, 0, tmpBuffer, 0, allBytesBuffer.length);
//                allBytesBuffer = tmpBuffer;
//            }
//            
//            for (byte by : bytesBuffer) {
//                System.out.print(by);
//            }
//            System.out.println();  
//        }
        

        // LIRE PAQUET DE BYTE PAR PAQUET DE BYTE
        final int BUFFER_CAPACITY = 4096; // 1ko => 1024, 4k => 4096
        
        byte[] allBytesBuffer = new byte[BUFFER_CAPACITY];
        int nextPosition = 0;
                  
        byte[] singleReadBuffer = new byte[BUFFER_CAPACITY];
        int numberOfBytesRead;
        
        // il est également possible de lire directement dans le allBytesBuffer en spécifiant un offset pour l'écriture
        while((numberOfBytesRead = System.in.read(singleReadBuffer)) != -1) {
            
            // ensure capacity
            int remainingSpace = allBytesBuffer.length - nextPosition;
            if(remainingSpace < numberOfBytesRead) {
                byte[] tmpBuffer = new byte[allBytesBuffer.length + BUFFER_CAPACITY];
                System.arraycopy(allBytesBuffer, 0, tmpBuffer, 0, allBytesBuffer.length);
                allBytesBuffer = tmpBuffer;
            }

            // copy form singleReadBuffer => allBytesBuffer
            System.arraycopy(singleReadBuffer, 0, allBytesBuffer, nextPosition, numberOfBytesRead);
            nextPosition += numberOfBytesRead; 
        } 
        
        
//        
//        try {
//            System.out.println(System.in.read());
//            
//            // lire tous les bytes de System.in jusqu'à un retour à la ligne
//            // les stocker dans une String
//            // afficher cette String 
//            
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("done");
        

    }

}
