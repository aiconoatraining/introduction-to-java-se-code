package com.thomas_gros.javase.io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataStreamMain {

    public static void main(String[] args) throws IOException {
        
//        String s = "bonjour";
//        InputStream is = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_16));
//        
//        //FileInputStream fis = new FileInputStream("/Users/thomasgros/Documents/Courses/test-data/file.html");
//        
//        DataInputStream dis = new DataInputStream(is);
//        
//        char c;
//        while(true) {
//            c = dis.readChar();
//            System.out.print(c);
//        }
        
        
        FileOutputStream fos = new FileOutputStream("/Users/thomasgros/Documents/Courses/test-data/file.html");
        DataOutputStream dos = new DataOutputStream(fos);
         
        dos.writeDouble(1.234);
        dos.writeChar('c');
        
        FileInputStream fis = new FileInputStream("/Users/thomasgros/Documents/Courses/test-data/file.html");
        DataInputStream dis = new DataInputStream(fis);
        
        System.out.println(dis.readDouble());
        System.out.println(dis.readChar());
        
        
        
        
        

        
        

    }

}
