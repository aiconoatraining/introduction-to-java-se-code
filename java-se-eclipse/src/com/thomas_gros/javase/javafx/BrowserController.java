package com.thomas_gros.javase.javafx;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class BrowserController implements Initializable {

    @FXML
    private Button goButton;
    
    @FXML
    private TextField adressTextField;
    
    @FXML
    private WebView webView;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // webView.getEngine().load("http://www.oracle.com");
        
        goButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                
                webView.getEngine().load(adressTextField.getText());
                
            }
        });
        
    }
    
}
