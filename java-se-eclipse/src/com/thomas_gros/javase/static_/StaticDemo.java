package com.thomas_gros.javase.static_;

public class StaticDemo {

    static {
        System.out.println("Hello !");
        System.out.println("Je suis un initializer statique !");
    }

    // class | static field
    public static String myString = "hello";
    public static final String MY_CONSTANT = "I'm a constant";

    // class | static method
    public static void myMethod() {
        System.out.println("Dans myMethod");
        // myMethodNonStatic(); // impossible, doit être invoquée sur un objet
    }

    public void myMethodNonStatic() {
        System.out.println("Dans myMethodNonStatic");
    }

    public static void main(String[] args) {

        System.out.println(StaticDemo.myString);
        StaticDemo.myMethod();

    }
}
