package com.thomas_gros.javase.oop.inheritance.shape;

public abstract class Shape {

    public enum Origin {
        TOP_LEFT(0, 0), TOP_RIGHT(1, 0), BOTTOM_LEFT(0, 1), BOTTOM_RIGHT(1, 1), CENTER(0.5, 0.5);

        private double x;
        private double y;

        private Origin(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

    }

    // public static final double[] ORIGIN_TOP_LEFT = { 0 , 0 };
    // public static final double[] ORIGIN_TOP_RIGHT = { 1 , 0 };
    // public static final double[] ORIGIN_BOTTOM_LEFT = { 0 , 1 };
    // public static final double[] ORIGIN_BOTTOM_RIGHT = { 1 , 1 };
    // public static final double[] ORIGIN_CENTER = { 0.5, 0.5 };

    private Point origin;
    // private double[] anchor = ORIGIN_TOP_LEFT;
    private Origin anchor = Origin.TOP_LEFT;

    public Shape(int x, int y) {
        this.origin = new Point(x, y);
    }

    public void setAnchor(Origin anchor) {
        this.anchor = anchor;
    }

    // public void setAnchor(double[] anchor) {
    // this.anchor = anchor;
    // }

    public Point getOrigin() {
        return origin;
    }

    public void setOrigin(Point origin) {
        this.origin = origin;
    }
}
