package com.thomas_gros.javase.oop.inheritance.shape;

public class ShapeMain {

    public static void main(String[] args) {

        Rectangle rect = new Rectangle();

        rect.setWidth(300);
        rect.setHeight(200);        
        
        Rectangle rect2 = new Rectangle(0, 0, 300, 200);
//        rect2.setAnchor(Shape.ORIGIN_CENTER);
        rect2.setAnchor(Shape.Origin.CENTER);

        Circle circle = new Circle();
    }

}
