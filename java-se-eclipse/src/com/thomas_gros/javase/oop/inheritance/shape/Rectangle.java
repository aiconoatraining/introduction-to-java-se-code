package com.thomas_gros.javase.oop.inheritance.shape;

public class Rectangle extends Shape {

    private int width;
    private int height;

//    public Rectangle() {
// BERK !
//        super(0, 0);
//        this.width = 0;
//        this.height = 0;
//    }

    
    public Rectangle() {
        this(0,0,0,0);
    }
    
    public Rectangle(int x, int y, int width, int height) {
        super(x,y);
//        this.x = x; // impossible car private => changer en protected ou créer constructeur sur classe Shape
//        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    
    
    
    
}
