package com.thomas_gros.javase.oop.inheritance;

public class Cat extends Animal {
    public Cat() {
        super();
    }
    
    @Override
    public void makeSomeSound() {
        System.out.println("Miaou");
    }
}