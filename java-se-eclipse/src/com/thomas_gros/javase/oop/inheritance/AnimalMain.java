package com.thomas_gros.javase.oop.inheritance;

public class AnimalMain {

    public static void main(String[] args) {        
        Dog dog = new Dog();
        dog.setAge(8);
        Cat cat = new Cat();
        cat.setAge(3);
        
        System.out.println(dog.getAge());
        System.out.println(cat.getAge()); 
         
        Animal a = new Dog(); // tous les Dog sont des Animal
        Animal a2 = new Cat(); // tous les Cat sont des Animal 

        // IMPOSSIBLE !!! Tous les animaux ne sont pas des Cat
        // par exemple un Dog n'est pas un Cat
        //Cat c = new Animal(); // Cat c = new Dog();
        
        Object a3 = new Cat();
        if(a3 instanceof Animal) {
         // il est possible de cast vers un sous-type, 
         // vérifier au préalable que c'est bon via instanceof
         // ESSAYER DE NE JAMAIS CASTER
            Animal a4 = (Animal) a3; 
        }


        Animal[] animals = { new Dog(), new Cat(), new Dog() };
        
        
        // OVERRIDING | SURCHARGE
        System.out.println(dog.toString());
        Animal a6 = dog;
        
        System.out.println(a6.toString()); // toString de Dog
        System.out.println(a6); // toString de Dog        
        
        
       // Animal myAnimal = new Animal();
        
        
    }

}
