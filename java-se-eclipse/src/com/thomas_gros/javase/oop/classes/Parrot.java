package com.thomas_gros.javase.oop.classes;

import java.util.Random;

public class Parrot {

    // ATTRIBUTS | CHAMPS | FIELDS
    private String name;
    private int age;

    // CONSTRUCTEURS

    // le constructeur sans paramètres est généré par le compilateur uniquement
    // si aucun autre constructeur n'est défini
    public Parrot() {
        name = null;
        age = 0;
    }

    public Parrot(String name, int age) {
        // this fait référence à l'objet courant
        this.name = name;
        this.age = age;
    }

    // INSTANCE METHODS
    // accesseurs
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // autres méthodes d'instance
    // public void repeatAfterMe(String[] words) {

    // Java 7 variadic arguments
    public void repeatAfterMe(String... words) {
        for (String w : words) {
            System.out.println(w);
        }
    }

    // CLASS | STATIC METHODS
    // quand le contexte d'un objet n'est pas nécessaire
    // exemples: méthodes main, méthodes utilitaires Math.cos, Math.sin,...

    /**
     * Transforme une String en mot parlé par un perroquet ! Cad, elle répète un
     * nombre aléatoire de fois (entre 1 et 5) les voyelles du mot
     * 
     * Par exemple Parrot.parrotize("hello") pourra retourner heellooooo ou
     * alors heeelloo
     * 
     * @param word
     * @return String mot passé en paramètre avec les voyelles répétées un
     *         nombre aléatoire de fois entre 1 et 5
     */
    public static String parrotize(String word) {

        // declarer une variable parrotizedWord = "";
        StringBuilder parrotizedWord = new StringBuilder();

        Random random = new Random();

        // parcourir tous les caractères de word
        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);

            // si voyelle
            if (currentChar == 'a' || currentChar == 'A' || currentChar == 'e' || currentChar == 'E'
                    || currentChar == 'u' || currentChar == 'U' || currentChar == 'i' || currentChar == 'I'
                    || currentChar == 'o' || currentChar == 'O' || currentChar == 'y' || currentChar == 'Y') {

                int repeat = random.nextInt(5) + 1;
                for (int j = 0; j < repeat; j++) { // parrotizedWord +=
                                                   // voyelle*random(1..5)
                    parrotizedWord.append(currentChar);
                }
            } else { // si consonne =>
                parrotizedWord.append(currentChar); // parrotizedWord +=
                                                    // consonne
            }
        }

        return parrotizedWord.toString();
    }

}
