package com.thomas_gros.javase.oop.classes;

public class ParrotMain {

    public static void main(String[] args) {
//        Parrot p = new Parrot();
//        p.setName("Coco");
//        p.setAge(28);

        Parrot p = new Parrot("Coco", 28);
        System.out.println(p.getAge());
        System.out.println(p.getName());

        String[] words = { "Hello", "World" };
        p.repeatAfterMe(words); // les paramètres variadic peuvent être groupés dans un tableau
        p.repeatAfterMe("Java", "is", "cool"); // ou déclarés comme un nombr infini de paramètres

        System.out.println(Parrot.parrotize("hello"));
    }

}
