package com.thomas_gros.javase.oop;

public class Lamp {

    // pas d'accès au champ depuis l'extérieur de la classe Lamp
    private boolean on;

    // accesseur en lecture.
    // isXXX pour boolean, getXXX sinon
    public boolean isOn() {
        return on;
    }

    // accesseur en écriture
    public void setOn(boolean on) {
        this.on = on;
    }

}