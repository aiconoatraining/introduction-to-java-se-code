package com.thomas_gros.javase.oop.interface_;

public interface BankService {
    void transfer(String sourceAccount,
                  String destinationAccount,
                  int amountToTransfer);
}