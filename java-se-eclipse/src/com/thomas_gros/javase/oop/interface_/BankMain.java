package com.thomas_gros.javase.oop.interface_;

import java.lang.reflect.InvocationTargetException;

public class BankMain {
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static BankService getBankService(String className) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Class clazz = Class.forName(className);
        if(! BankService.class.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException(String.format("%s does not implement BankService", className));
        }
           
        return ((Class<? extends BankService>) clazz).newInstance();  
    }
    
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        // chargement dynamique d'un service, passé en paramètre du programme à l'exécution,
        // par exemple com.thomas_gros.javase.oop.interface_.VisaService
        BankService bankService = getBankService(args[0]);

        bankService.transfer("12345ABC", "45678DEF", 125);
    }
}