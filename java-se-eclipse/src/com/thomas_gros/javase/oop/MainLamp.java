package com.thomas_gros.javase.oop;

public class MainLamp {

	public static void main(String[] args) {

        // declaring and assigning on the same line
		Lamp myLamp = new Lamp();

		System.out.println(myLamp.isOn());

		myLamp.setOn(true);

		System.out.println(myLamp.isOn());
	}

}