package com.thomas_gros.javase.generics;

import java.util.ArrayList;
import java.util.List;

import com.thomas_gros.javase.collections.Person;

public class GenericsMain {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("12345", "Thomas", "Gros"));

//        persons.add("coucou"); // The method add(Person) in the type List<Person> is not applicable for the arguments (String)
//        persons.add(new Integer(42)); // The method add(Person) in the type List<Person> is not applicable for the arguments (Integer)
        
    }

}
