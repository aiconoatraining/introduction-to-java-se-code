package com.thomas_gros.javase.generics;

import java.util.Collection;
import java.util.List;

public class BoxMain {

    public static void main(String[] args) {
        // Box<String> box = new Box<String>();
        Box<String> box = new Box<>(); // inférence de type, opérateur diamond
        box.setContent("je suis le contenu de la boite");
        
        Box<Integer> box2 = new Box<>();
        box2.setContent(new Integer(42));
        box2.setContent(12); // => new Integer(12)  (BOXING)
        
        
        Box box3 = new Box(); // Raw Type  ~ Box<Object>
        box3.setContent(new Integer(12));
        box3.setContent("hello !!!");
        
        box.<String>genericMethod("hello");
        box.genericMethod("hello"); // inférence de type aussi
        // box.<String>genericMethod(new Integer(42)); // The parameterized method <String>genericMethod(String) of type Box<String> is not applicable for the arguments (Integer)
        
        box.boundedTypeParameter(12); // 12 => new Integer(12), subclass de Number
        box.boundedTypeParameter(12.56); // 12.56 => new Double(12.56), subclass de Number
        // box.boundedTypeParameter(new Object()); // Object n'est pas une subclass de Number
          
        Number number;
        Integer integer = 12;
        number = integer; // Integer subtype de Number
        
        Box<Number> boxNumber;
        Box<Integer> boxInteger = new Box<Integer>(); // n'est pas un subtype de Box<Number>
        // boxNumber = boxInteger; // Type mismatch: cannot convert from Box<Integer> to Box<Number>
        
        // Il est possible d'écrire public interface List<E> extends Collection<E>
        
        
        
        

    }

}
