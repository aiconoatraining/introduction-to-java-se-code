package com.thomas_gros.javase.generics;

public class Box<T> {

    private T content;
    
    public T getContent() {
        return content;
    }
    
    public void setContent(T content) {
        this.content = content;
    }
    
    public <U> void genericMethod(U u) {  
    }

    public <U extends Number> void boundedTypeParameter(U u) {
        
    }
    
}