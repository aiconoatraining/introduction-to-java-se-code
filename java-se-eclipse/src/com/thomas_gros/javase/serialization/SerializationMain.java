package com.thomas_gros.javase.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        
// Sérialiser un objet = convertir son état en un flux de bytes
// qui peut être inversé pour reconstruire une copie de l'objet
        
        // sérialization: object => bytes
        // dé-sérialization: bytes => object
        
        
// Un objet est serializable SI sa classe ou une de ses superclass implémentent
// l'interface Serializable
        
// Java fournit un mécanisme par défaut pour sérializer les classes Serializable.
       
        FileOutputStream fos = new FileOutputStream("/Users/thomasgros/Documents/Courses/test-data/myobject.data");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        
        MyClass myClass = new MyClass("hello world", 123456);
        oos.writeObject(myClass);
        
        
        FileInputStream fis = new FileInputStream("/Users/thomasgros/Documents/Courses/test-data/myobject.data");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object o = ois.readObject();
        if(o instanceof MyClass) {
            MyClass myObjectFromFile = (MyClass) o;
            System.out.println(myObjectFromFile);
        }

    }

}
