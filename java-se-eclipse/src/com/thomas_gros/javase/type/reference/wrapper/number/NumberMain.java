package com.thomas_gros.javase.type.reference.wrapper.number;

public class NumberMain {

	public static void main(String[] args) {
		byte b = 3;

		Byte myByte = new Byte(b);

		Double myDouble = new Double(2.5);

		Integer myInt = new Integer(1);
		double myIntConvertToDouble = myInt.doubleValue();

		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		System.out.println(Integer.BYTES);

		int myIntFromString = Integer.parseInt("12345");
		System.out.println(myIntFromString);

		// int myIntFromString2 = Integer.parseInt("1a2b3c4d5e"); //
		// java.lang.NumberFormatException
		// System.out.println(myIntFromString2);

		// AUTOBOXING // UNBOXING
		// Integer k = new Integer(18);
		// int i = k.intValue();
		int i = new Integer(18); // unboxing = type reference => type primitif

		// Integer j = new Integer(i);
		Integer j = i; // autoboxing = type primitif => type reference

	}

}
