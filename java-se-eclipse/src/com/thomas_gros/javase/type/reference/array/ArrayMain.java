package com.thomas_gros.javase.type.reference.array;

public class ArrayMain {

	public static void main(String[] args) {

		// syntaxe avec new
		int[] intArray = new int[10]; 		// la taille d'un tableau est fixée à l'initialisation
		// int intArray[] = new int[10]; // syntaxe possible mais non recommandée
		
		// syntaxe literale
		int[] myOtherIntArray = { 10, 20, 30, 40, 50 }; // [10, 20, 30, 40, 50]
		System.out.println(myOtherIntArray[0]); // 10
		
		// longueur du tableau
		System.out.println(intArray.length); // 10
		
		// lecture
		System.out.println(intArray[0]);
		System.out.println(intArray[1]);
		System.out.println(intArray[9]);
		System.out.println(intArray[intArray.length - 1]);
		// System.out.println(intArray[10]); // java.lang.ArrayIndexOutOfBoundsException	
		
		// écriture
		intArray[0] = 42;
		System.out.println(intArray[0]);
		
		
		// tableaux a multiples dimensions
		int[][] twoDimensionsIntArray = new int[3][3];
		/*
		 * [ [0,0,0], [0,0,0], [0,0,0] ]
		 * 
		 * twoDimensionsIntArray[0] => [0,0,0]
		 * 
		 * [ [0,0,0], 
		 *   [0,0,0], 
		 *   [0,0,0] ]
		 */
		
		twoDimensionsIntArray[1][1] = 42;
		
		/*
		 * [ [0,0,0], [0,42,0], [0,0,0] ]
		 * 
		 * [ [0,0,0], 
		 *   [0,42,0], 
		 *   [0,0,0] ]
		 */
		
		int[][] twoOtherDimensions = {
				{ 1, 2 },
				{ 3, 4, 5 },
				{ 6, 7, 8, 9 },
				{ 0 }
		};
		
		System.out.println(twoOtherDimensions[0][0]); // 1
		System.out.println(twoOtherDimensions[1][2]); // 5
		
		byte[] input = { 0b1101, 0b1001, 0b11, 0b11011 };
		byte[] output = new byte[2];

		System.arraycopy(input, 0, output, 0, 2);
		System.out.println(output[0]);
		System.out.println(output[1]);
		
	}

}
