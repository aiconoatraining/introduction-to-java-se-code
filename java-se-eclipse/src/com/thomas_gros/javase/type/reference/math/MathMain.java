package com.thomas_gros.javase.type.reference.math;

import java.util.Random;

public class MathMain {

	public static void main(String[] args) {
		// constantes
		System.out.println(Math.PI);
		System.out.println(Math.E);
		
		// trigo
		System.out.println(Math.cos(5));
		
		// min / max
		System.out.println(Math.max(3, 5));
		
		// random
		System.out.println(Math.random()); // préférer java.util.Random
		
//		java.util.Random random = new java.util.Random();
		Random random = new Random(); // CTRL + SHIFT + o
		System.out.println(random.nextInt());
		
		// arrondis
		System.out.println(Math.ceil(12.3));
		System.out.println(Math.floor(12.3));
		System.out.println(Math.round(12.3));

	}

}
