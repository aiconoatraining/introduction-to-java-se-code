package com.thomas_gros.javase.type.reference.scanner;

import java.util.Scanner;

public class ScannerMain {

	public static void main(String[] args) {
		// System.in  // trop bas niveau
		// System.console() // mauvaise intégration dans IDEs
		
		Scanner scanner  = new Scanner(System.in); // java.util.Scanner
		
		String line;
		
		do {		
			line = scanner.nextLine();
			System.out.println("input: " + line);
		} while ( ! "quit".equals(line) );
		
	}
}
