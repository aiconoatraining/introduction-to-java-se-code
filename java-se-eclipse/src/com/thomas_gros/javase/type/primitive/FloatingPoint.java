package com.thomas_gros.javase.type.primitive;

public class FloatingPoint {

	public static void main(String[] args) {
		
		// double
		// double-precision 64-bit IEEE 754 floating-point
		// par default 0.0d
		double myDouble = 123;
		double myDouble2 = 123.45;
		double myDouble3 = 123e2;
		double myDouble4 = 123d;
		
		// float
		// single-precision 32-bit IEEE 754 floating-point
		// par default 0.0f
		float myFloat = 123;
		float myFloat2 = 123.54f;
				
	}

}
