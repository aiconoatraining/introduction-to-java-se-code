package com.thomas_gros.javase.object;

import com.thomas_gros.javase.collections.Person;

public class ObjectMain {
    public static void main(String[] args) {
        
        String toto = "toto";
        String tata = "tata";
        
        System.out.println(toto == tata); // identité des références 
        System.out.println(toto.equals(tata));
        
        System.out.println(toto.hashCode());
        
        
        Person p1 = new Person("12345", "Thomas", "Gros");
        Person p2 = new Person("98765", "Bob", "Dylan");
        
        p1.equals(p1); // p1 == p1
        p1.equals(p2); // p1 == p2
        
        
    }
}
