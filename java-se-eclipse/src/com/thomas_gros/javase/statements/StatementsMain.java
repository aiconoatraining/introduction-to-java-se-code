package com.thomas_gros.javase.statements;

public class StatementsMain {

	public static void main(String[] args) {
		
		// if - else if - else
		
		int 	number = 4;
		
		if(number < 2) {
			System.out.println("small");
		} else if (number >= 2 && number <= 5) {
			System.out.println("medium");
		} else { // dans tous les autres cas
			System.out.println("something else");
		}
		
		
//		if(number == 1) {
//			
//		} else if (number == 2) {
//			
//		} else if (number == 4) {
//			
//		} else {
//			
//		}
		
		switch (number) {
		case 1:
			System.out.println("very small");
			break;
		case 2:
			System.out.println("medium");
			break;
		case 4:
			System.out.println("medium");
			break;
		default:
			System.out.println("something else");
			break;
		}
		
		String myString = "hello";
		switch(myString) {
		case "hello": 
			// do something if myString is equal to "hello"
			break;
		case "foo": 
			// do something if myString is equal to "foo"
			break;
		default:
			// do something if myString is equal to something else
			break;
		}
		
		
		
		int[] myIntArray = { 1, 2, 3, 4, 5 };
		for (int i = 0; i < myIntArray.length; i++) {
			System.out.println(myIntArray[i]);
		}
		
		// le enhanced for est intéressant pour itérer séquentiellement sur des Iterables
		// voir http://docs.oracle.com/javase/1.5.0/docs/guide/language/foreach.html
		for (int value : myIntArray) {
			System.out.println(value);
		}
		
		
		
		

	}

}
