package com.thomas_gros.javase.exception;

public class ExceptionMain {

    public static void main(String[] args) /* throws Exception */ {
        
        try {
            int[] tab = new int[0];
            tab[1] = 2;
        } catch(ArrayIndexOutOfBoundsException ex ) {
            
        } catch(Exception ex) {
            
        }
        
        // Mécanisme utilisé pour gérer les erreurs et les évènements
        // exceptionnels:
        // - erreurs du développeur, tapper en dehors des limites d'un tableau
        // - perte de connexion réseau, crash de disque dur, ...
    
        Network network = new Network();
        
        try {
              network.doSomethingWithTheNetworkChecked();
        } catch(Exception ex) {
            System.out.println("network operation failure");
            System.out.println(ex);
        } finally {
            System.out.println("S'exécute dans tous les cas");
        }
        
        System.out.println("network operation => done");
        
        network.doSomethingWithTheNetworkUnchecked();

    }

}
