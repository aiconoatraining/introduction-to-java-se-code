package com.thomas_gros.javase.exception;

public class Network {

    // throws ou try catch obligatoire (catch or specify) car Checked Exception
    /**
     * @throws Exception if a network problem occurs
     */
    public void doSomethingWithTheNetworkChecked() throws Exception { 
        if(Math.random() > 0.5) {
            throw new Exception("something bad happened with the network connection...");
        }
        System.out.println("doSomethingWithTheNetwork => done");
    }
    
    // throws ou try-catch non obligatoire car Unchecked Exception    
    /**
     * @throws RuntimeException if a network problem occurs
     */
    public void doSomethingWithTheNetworkUnchecked() {
        if(Math.random() > 0.5) {
//            throw new RuntimeException("something bad happened with the network connection...");
            throw new NetworkAccessException("something bad happened with the network connection...");
        }  
        
        System.out.println("doSomethingWithTheNetworkUnchecked => done");
    }
    
}
