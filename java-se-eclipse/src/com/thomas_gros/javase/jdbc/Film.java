package com.thomas_gros.javase.jdbc;

public class Film {

    private int id;
    private String title;
    private int languageId;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public int getLanguageId() {
        return languageId;
    }
    
    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "Film [id=" + id + ", title=" + title + ", languageId=" + languageId + "]";
    }
    
    
    
}
