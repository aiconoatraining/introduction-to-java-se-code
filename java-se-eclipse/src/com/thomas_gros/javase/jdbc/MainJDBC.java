package com.thomas_gros.javase.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MainJDBC {

    private static final String SAKILA_URL = "jdbc:mysql://localhost:3306/sakila";
    
    
    public static Film mapFilmFromRs(ResultSet rs) throws SQLException {
        Film f = new Film();
        f.setId(rs.getInt("film_id"));
        f.setTitle(rs.getString("title"));
        f.setLanguageId(rs.getInt("language_id"));
        return f;
    }
    
    
    public static void main(String[] args) {
        
        Properties properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "root");

//        Connection conn = null;
//        try {
//            conn = DriverManager.getConnection(SAKILA_URL, properties);
//            
//            Statement stmt = conn.createStatement();
//            
//            // stmt.executeQuery() // => SELECT
//            // stmt.executeUpdate() // => INSERT, UPDATE, DELETE
//            
//            ResultSet rs = stmt.executeQuery("SELECT * FROM sakila.film");
//            while(rs.next()) {
//                System.out.println(String.format("%d %s", rs.getInt(1), rs.getString("title")));
//            }
//            
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            if(conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        
        
        // try-with-ressources
        try(Connection conn = DriverManager.getConnection(SAKILA_URL, properties)) {
            Statement stmt = conn.createStatement();
            
            // stmt.executeQuery() // =>  SELECT
            // stmt.executeUpdate() // => INSERT, UPDATE, DELETE
            
//            ResultSet rs = stmt.executeQuery("SELECT * FROM sakila.film");
//            while(rs.next()) {
//                System.out.println(String.format("%d %s", rs.getInt(1), rs.getString("title")));
//            }
            
            int id = 3;
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM sakila.film WHERE film_id = ?");
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                System.out.println(String.format("%d %s", rs.getInt(1), rs.getString("title")));
            }            
                       
            // insérer un nouveau film
            Film film = new Film();
            film.setTitle("Mon super film");
            film.setLanguageId(1);

            String newFilmQuery = "INSERT INTO sakila.film (title, language_id) VALUES (?, ?)";
            pstmt = conn.prepareStatement(newFilmQuery, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, film.getTitle());
            pstmt.setInt(2, film.getLanguageId());
            pstmt.executeUpdate();
            
            ResultSet generatedKeysRs = pstmt.getGeneratedKeys();
            generatedKeysRs.next();
            int generatedId = generatedKeysRs.getInt(1);
            System.out.println(String.format("generatedId: %d", generatedId));
            
            
            String findFilm = "SELECT * FROM sakila.film WHERE film_id = ?";
            pstmt = conn.prepareStatement(findFilm);
            pstmt.setInt(1, generatedId);
            ResultSet verificationRs = pstmt.executeQuery();
            verificationRs.next();
            
            Film filmFromDb = mapFilmFromRs(verificationRs);
            
            
            System.out.println(filmFromDb);
            
            // RECUPERER SOUS FORME D'UNE List<Film> les films dont le titre commence par un E
            String filmsStartingWithQuery = "SELECT * FROM sakila.film WHERE title LIKE ?";
            pstmt = conn.prepareStatement(filmsStartingWithQuery);
            pstmt.setString(1, "E%");
            ResultSet filmsStartingWithERs = pstmt.executeQuery();
            
            List<Film> films = new ArrayList<>();
            while(filmsStartingWithERs.next()) {
                films.add(mapFilmFromRs(filmsStartingWithERs));
            }
            

        } catch (SQLException e) {
            e.printStackTrace();
        }
        

    }

}
