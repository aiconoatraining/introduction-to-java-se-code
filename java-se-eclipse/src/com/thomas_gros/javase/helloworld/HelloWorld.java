package com.thomas_gros.javase.helloworld; // always use a default package

// Class names should be nouns, in mixed case with the first letter of each internal word capitalized
// see http://www.oracle.com/technetwork/java/codeconventions-135099.html
public class HelloWorld {

	public static void main(String[] args) { // the programm entry point

		// type sysout then CTRL+SPACE inside Eclipse
		System.out.println("Hello, World !");

	}

}