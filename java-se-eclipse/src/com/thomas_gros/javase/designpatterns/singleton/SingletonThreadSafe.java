package com.thomas_gros.javase.designpatterns.singleton;

public class SingletonThreadSafe {

//    public static SingletonThreadSafe instance = new SingletonThreadSafe();
//    private SingletonThreadSafe() {}
    
    private final static SingletonThreadSafe instance = new SingletonThreadSafe();
    private SingletonThreadSafe() {}
    
    public static SingletonThreadSafe getInstance() {
        return instance;
    }
    
//    public static void doSomething() {
//        instance = new SingletonThreadSafe(); // impossible parce que final
//    }
}
