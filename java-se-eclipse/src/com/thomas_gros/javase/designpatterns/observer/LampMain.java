package com.thomas_gros.javase.designpatterns.observer;

public class LampMain {

    public static void main(String[] args) {
        
        // Subject
        Lamp lamp = new Lamp();
        
        // ConcreteObserverA
        CaptorType1 c1 = new CaptorType1();
        lamp.registerCaptor(c1);

        // ConcreteObserverB
        CaptorType2 c2 = new CaptorType2();
        lamp.registerCaptor(c2);
        
        // notifyObservers
        lamp.setOn(true); // a l'intérieur => notifie tous les capteurs enregistrés
        lamp.setOn(false); // a l'intérieur => notifie tous les capteurs enregistrés
        lamp.setOn(true); // a l'intérieur => notifie tous les capteurs enregistrés
        
        // TODO faire évoluer le programme:
        // les capteurs peuvent réagir à des lampes différentes
            // les capteurs sont notifiés de la lampe qui a émis l'evènement 
        Lamp lamp2 = new Lamp();
        lamp2.registerCaptor(c1);
        lamp2.setOn(true);

//        Button button;
//        
//        ButtonClickListener clickListener;
//        
//        button.addClickEventListener(clickListener);
//        button.addDoubleClickEventListener(anotherListener);
        
        
        
        
        
        
    }
}
