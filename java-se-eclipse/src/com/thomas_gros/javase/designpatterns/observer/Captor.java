package com.thomas_gros.javase.designpatterns.observer;

public interface Captor {

    void notify(Lamp lamp);

}
