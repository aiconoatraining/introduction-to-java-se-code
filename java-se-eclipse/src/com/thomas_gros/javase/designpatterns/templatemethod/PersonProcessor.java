package com.thomas_gros.javase.designpatterns.templatemethod;

public abstract class PersonProcessor {

    // template method, see https://en.wikipedia.org/wiki/Template_method_pattern
    public final void processPersons(Person[] persons) {
        
        // 1) filtrer
        Person[] filteredPersons = filter(persons);
        
        // 2) appliquer un traitement sur les personnes filtrées
        process(filteredPersons);
        
        done();
        
    }

    protected abstract Person[] filter(Person[] persons);
    protected abstract void process(Person[] filteredPersons);
    
    private final void done() {
        System.out.println("done");
    }
    
}
