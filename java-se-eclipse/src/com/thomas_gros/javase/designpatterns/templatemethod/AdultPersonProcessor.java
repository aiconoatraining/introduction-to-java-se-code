package com.thomas_gros.javase.designpatterns.templatemethod;

public class AdultPersonProcessor extends PersonProcessor {
    
    @Override
    protected Person[] filter(Person[] persons) {
        System.out.println("filtrer les adultes");
        return persons;
    }

    @Override
    protected void process(Person[] filteredPersons) {
        System.out.println("traiter les adultes");
    }

    
    
}
