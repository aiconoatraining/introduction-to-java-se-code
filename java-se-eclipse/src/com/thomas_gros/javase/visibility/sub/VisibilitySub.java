package com.thomas_gros.javase.visibility.sub;

public class VisibilitySub {

    // visible depuis l'intérieur de la classe uniquement
    private String privateString = "private";
    // visible depuis le même package
    String packagePrivateString = "package-private";
    // visible depuis le même package ET visible depuis une sous-classe
    protected String protectedString = "protected";
    // visible de partout
    public String publicString = "public";
    
}
