package com.thomas_gros.javase.visibility;

import java.lang.reflect.Field;

import com.thomas_gros.javase.visibility.sub.VisibilitySub;

public class VisibilityMain {

    public static void main(String[] args) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Visibility v = new Visibility();
        // System.out.println(v.privateString); // non visible depuis l'extérieur de l'objet / classe         
        System.out.println(v.packagePrivateString);
        System.out.println(v.protectedString);
        System.out.println(v.publicString);
        
        VisibilitySub vSub = new VisibilitySub();
        System.out.println(vSub.publicString);
        
        
        // getClass() => représentation Objet d'une Classe
        // Reflection API  java.lang.reflect
        Field field = v.getClass()
                        .getDeclaredField("privateString");
        field.setAccessible(true);
        
        System.out.println(field.get(v));
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }

}
