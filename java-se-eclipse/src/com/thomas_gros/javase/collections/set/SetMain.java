package com.thomas_gros.javase.collections.set;

import java.util.HashSet;
import java.util.Set;

import com.thomas_gros.javase.collections.Person;

public class SetMain {
    public static void main(String[] args) {
        Person p1 = new Person("12345", "Thomas", "Gros");
        Person p2 = new Person("98765", "Bob", "Dylan");
        Person p3 = new Person("34567", "Mick", "Jagger");
        
        // Collection qui ne contient pas de doublons
        Set<Person> persons = new HashSet<Person>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        
        System.out.println(persons);        
        System.out.println(persons.size());
        
        persons.add(p3); // pas de doublons
        System.out.println(persons);        
        System.out.println(persons.size());
      
        System.out.println(persons.contains(p3));
        
        for (Person person : persons) {
            System.out.println(person);
        }
    }
}
