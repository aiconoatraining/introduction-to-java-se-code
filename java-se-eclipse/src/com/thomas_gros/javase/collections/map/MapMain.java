package com.thomas_gros.javase.collections.map;

import java.util.HashMap;
import java.util.Map;

public class MapMain {

    public static void main(String[] args) {
        
           // Un objet qui associe des clés à des valeurs.
           // Les clés ne peuvent pas être en doublon.
           // Chaque clé est associé à au plus une valeur.
           Map<String, String> map = new HashMap<String, String>();
           
           map.put("Java SE", "Pour développer des applications console, desktop, etc...");
           map.put("Java EE", "Pour développer des applications distribuées, web, etc...");
           map.put("JVM", "La machine virtuelle Java !");

           System.out.println(map.get("Java SE"));
           System.out.println(map.get("Java EE"));
           System.out.println(map.get("JVM"));
           

    }

}
