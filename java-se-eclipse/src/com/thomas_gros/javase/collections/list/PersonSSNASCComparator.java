package com.thomas_gros.javase.collections.list;

import java.util.Comparator;

import com.thomas_gros.javase.collections.Person;

public class PersonSSNASCComparator implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        
        return p1.getSsn().compareTo(p2.getSsn());

    }
    
}
