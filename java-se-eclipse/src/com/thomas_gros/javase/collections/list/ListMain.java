package com.thomas_gros.javase.collections.list;

import java.util.ArrayList;
import java.util.List;

import com.thomas_gros.javase.collections.Person;

public class ListMain {
    public static void main(String[] args) {

        Person p1 = new Person("12345", "Thomas", "Gros");
        Person p2 = new Person("98765", "Bob", "Dylan");
        Person p3 = new Person("34567", "Mick", "Jagger");
        
        // List = collection ordonnée d'éléments (aussi appelée séquence)
        // autorise typiquement les doublons
        // http://docs.oracle.com/javase/8/docs/api/java/util/List.html
        
        List<Person> persons = new ArrayList<Person>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);

        System.out.println(persons);
        System.out.println(persons.size());
        
        for (Person person : persons) {
            System.out.println(person);
        }
        
        System.out.println(persons.get(1));
        System.out.println(persons.contains(p3));
        System.out.println(persons.indexOf(p1));
        
        persons.sort(new PersonSSNASCComparator());
        System.out.println(persons);

        persons.sort(new PersonSSNDESCComparator());
        System.out.println(persons);
        
        persons.sort(new PersonSSNComparator(PersonSSNComparator.ASC));
        System.out.println(persons);
        
        persons.sort(new PersonSSNComparator(PersonSSNComparator.DESC));
        System.out.println(persons);
        

    }
}
