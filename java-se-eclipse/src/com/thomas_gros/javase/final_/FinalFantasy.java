package com.thomas_gros.javase.final_;

public class FinalFantasy {

    public static final String JE_SUIS_UNE_CONSTANTE = "hello";

    public static void main(String[] args) {

        System.out.println(Math.PI);
        System.out.println(FinalFantasy.JE_SUIS_UNE_CONSTANTE);

        final int a = 3;
        // a = 4; // final variable cannot be assigned

        final int b;
        b = 5; // ok

    }

}
