package com.thomas_gros.javase.enumeration;

public class ColorsMain {
    public static void main(String[] args) {
        Color myColor = Color.RED;
        Color myColor2 = Color.BLUE;
        Color myColor3 = Color.GREEN;   

        for(Color c: Color.values()) {
            System.out.println(c);
        }
    }
}
