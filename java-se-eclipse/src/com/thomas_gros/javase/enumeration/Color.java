package com.thomas_gros.javase.enumeration;

public enum Color {

    RED("FF0000"), GREEN("00FF00"), BLUE("0000FF");
    
    private Color(String hexValue) {
        this.hexValue = hexValue;
    }
    
    private String hexValue;
    
    public String getHexValue() {
        return hexValue;
    }
    
}
