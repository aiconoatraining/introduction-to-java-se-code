package com.thomas_gros.javase.filrouge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Slide implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private ArrayList<SlideElement<?>> slideElements;

    public Slide() {
        // inférence de type, operateur diamond <> depuis Java 7
        slideElements = new ArrayList<>();
    }

    /**
     * @throws NullPointerException if slideElement is null
     */
    public void addSlideElement(SlideElement<?> slideElement) {
        Objects.requireNonNull(slideElement, "slideElement must not be null");
        slideElements.add(slideElement);
    }

    /**
     * @throws NullPointerException if slideElement is null
     */
    public void removeSlideElement(SlideElement<?> slideElement) {
        Objects.requireNonNull(slideElement, "slideElement must not be null");
        slideElements.remove(slideElement);
    }

    @Override
    public String toString() {
        return "Slide [slideElements=" + slideElements + "]";
    }
    
}
