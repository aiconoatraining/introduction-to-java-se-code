package com.thomas_gros.javase.filrouge;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Objects;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
	    
	    String[][] data = {
	            {
	                "Java c'est super !",
	                "http://www.example.com/img1.jpg"
	            },
                {
                    "Java EE c'est rigolo",
                    "http://www.example.com/img2.jpg"
                },
                {
                    "Spring c'est bien aussi !",
                    "http://www.example.com/img3.jpg"
                },
	    };
	    
	    Slideshow slideshow = new Slideshow();
	    
	    for (String[] d : data) {
	        Slide s = new Slide();
	        TextSlideElement tse = new TextSlideElement();
	        tse.setX(50);
	        tse.setY(50);
	        tse.setWidth(700);
	        tse.setHeight(50);
	        tse.setContent(d[0]);
	        s.addSlideElement(tse);
	        
	        ImageSlideElement ise = new ImageSlideElement();
	        ise.setX(50);
	        ise.setY(150);
	        ise.setWidth(700);
	        ise.setHeight(400);
	        ise.setContent(d[1]);
	        s.addSlideElement(ise);
	        
	        slideshow.addSlide(s);
        }

        SlideshowPlayer slideshowPlayer = new SlideshowPlayer(slideshow);

		// String line;  // non initialisée, devra l'être avant d'être utilisée une première fois.
		String line = null;
		
		Scanner scanner = new Scanner(System.in);
		do {
			
			line = scanner.nextLine();
			
			switch (line) {
			case "previous":
			    slideshowPlayer.previousSlide();
				break;
			case "next":
			    slideshowPlayer.nextSlide();
				break;
			case "play":
			    slideshowPlayer.play();
				break;
			case "pause":
			    slideshowPlayer.pause();
				break;
			case "stop":
			    slideshowPlayer.stop();
				break;
			case "quit":
			    slideshowPlayer.stop();
				System.out.println("Bye bye");
				break;
			case "save":
			    try {
			        save(slideshowPlayer.getSlideshow(), "holidays.slideshow");
			    } catch(IOException ex) {
			        System.out.println("Oops cannot save the slideshow");
			        ex.printStackTrace(); // logger l'exception
			    }
			    break;
            case "load":
                Slideshow loadedSlideshow;
                try {
                    loadedSlideshow = load("holidays.slideshow");
                    System.out.println("Loaded slideshow successfully");
                    System.out.println(loadedSlideshow);
                    slideshowPlayer.setSlideshow(loadedSlideshow);
                } catch (ClassNotFoundException e) {
                    System.out.println("Cannot load slideshow");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.out.println("Cannot read file");
                    e.printStackTrace();
                } catch(ClassCastException ex) {
                    System.out.println("The content of the file is not a slideshow");
                    ex.printStackTrace();
                }
                break;
			default:
				System.out.println("input: something else");
				break;
			}
			
		} while (! "quit".equals(line));		
	}
	
	/**
	 * 
	 * @throws NullPointerException if slideshow is null or if fileName is null
	 * @throws IOException if there is a problem writing the slideshow into the file.
	 */
	private static void save(Slideshow slideshow, String fileName) throws IOException {
	    Objects.requireNonNull(slideshow);
	    Objects.requireNonNull(fileName);
	    
	    FileOutputStream fos = new FileOutputStream(fileName);
	    BufferedOutputStream bos = new BufferedOutputStream(fos);
	    ObjectOutputStream oos = new ObjectOutputStream(bos);
	    oos.writeObject(slideshow);
	    oos.close();
	}
	
	
	/**
	 * 
     * @throws NullPointerException if fileName is null
     * @throws IOException if fileName cannot be read
	 * @throws ClassNotFoundException if the Slideshow class cannot be found at runtime
     * @throws RuntimeException if the content of fileName is not a Slideshow
	 */
	private static Slideshow load(String fileName) throws IOException, ClassNotFoundException {
	    Objects.requireNonNull(fileName);
	    
	    FileInputStream fis = new FileInputStream(fileName);
	    BufferedInputStream bis = new BufferedInputStream(fis);
	    ObjectInputStream ois = new ObjectInputStream(bis);
	    
	    Object o = ois.readObject();
	    ois.close();
	    
	    if(! (o instanceof Slideshow)) {
	        throw new ClassCastException(String.format("The content of %s is not a valid Slideshow", fileName));
	    }
	    
        return (Slideshow) o;
	}
	
	
	
	

}
