package com.thomas_gros.javase.filrouge;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class SlideshowPlayer {
    
    private static final int DELAY_BETWEEN_SLIDES = 1000;

    private Slideshow slideshow;
    private int currentSlideIndex;
    private boolean autoplay;
    
    private Timer timer;

    /**
     * @throws NullPointerException if slideshow is null
     */
    public SlideshowPlayer(Slideshow slideshow) {
        Objects.requireNonNull(slideshow);
        this.slideshow = slideshow;
        resetSlideshowPlayerState();
    }
    
    private void resetSlideshowPlayerState() {
        currentSlideIndex = 0;
        autoplay = false;
    }
    
    /**
     * @throws NullPointerException if slideshow is null
     */
    public void setSlideshow(Slideshow slideshow) {
        Objects.requireNonNull(slideshow);
        this.slideshow = slideshow;
        resetSlideshowPlayerState();
    }
    
    public Slideshow getSlideshow() {
        return slideshow;
    }

    public Slide getCurrentSlide() {
        return slideshow.getSlideAt(currentSlideIndex);
    }

    public int getCurrentSlideIndex() {
        return currentSlideIndex;
    }

    public Slide nextSlide() {
        if(currentSlideIndex < slideshow.size() - 1) {
            currentSlideIndex++;
        }
        
        System.out.println(slideshow.getSlideAt(currentSlideIndex));
        return slideshow.getSlideAt(currentSlideIndex);
    }

    public Slide previousSlide() {
        if(currentSlideIndex > 0) {
            currentSlideIndex--;
        }
        System.out.println(slideshow.getSlideAt(currentSlideIndex));
        return slideshow.getSlideAt(currentSlideIndex);
    }

    /**
     * @throws IndexOutOfBoundsException if index is negative or greater 
     * than the number of slides in the slideshow
     */
    public Slide goToSlideIndex(int index) {        
        if(index < 0
        || index >= slideshow.size()) {
            throw new IndexOutOfBoundsException(String.format("%d",index));
        }

        currentSlideIndex = index;
        
        System.out.println(slideshow.getSlideAt(currentSlideIndex));
        return slideshow.getSlideAt(currentSlideIndex);
    }

    public void play() {
        timer = new Timer();
        // Création d'une classe anonyme qui dérive de TimerTask
        TimerTask autoPlayTimerTask = new TimerTask() {
            
            @Override
            public void run() {
                nextSlide(); // SlideshowPlayer.this.nextSlide();
            }
        };
        
        timer.schedule(autoPlayTimerTask, DELAY_BETWEEN_SLIDES, DELAY_BETWEEN_SLIDES);

        // timer.schedule( () -> System.out.println("coucou") , 0, DELAY_BETWEEN_SLIDES);
        autoplay = true;
    }

    public void pause() {
        if(autoplay) { timer.cancel();}
        autoplay = false;
        System.out.println(slideshow.getSlideAt(currentSlideIndex));
    }

    public void stop() {
        if(autoplay) { timer.cancel(); }
        resetSlideshowPlayerState();
        System.out.println(slideshow.getSlideAt(currentSlideIndex));
    }

}
